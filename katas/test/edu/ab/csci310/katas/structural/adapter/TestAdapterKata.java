package edu.ab.csci310.katas.structural.adapter;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;

public class TestAdapterKata {

	@Test
	public void testFacultyUser() {
		User facultyUser = new FacultyUser();
		facultyUser.setId(FACULTY_ID);
		facultyUser.setFirstName(FACULTY_FIRST_NAME);
		facultyUser.setLastName(FACULTY_LAST_NAME);
		facultyUser.setGender(FACULTY_GENDER);
		facultyUser.setDateOfBirth(FACULTY_DATE);

		assertEquals(FACULTY_ID, facultyUser.getId());
		assertEquals(FACULTY_FIRST_NAME, facultyUser.getFirstName());
		assertEquals(FACULTY_LAST_NAME, facultyUser.getLastName());
		assertEquals(FACULTY_GENDER, facultyUser.getGender());
		assertEquals(FACULTY_DATE, facultyUser.getDateOfBirth());
		System.out.println(facultyUser.toString());
	}

	@Test
	public void testAdministrativeAssistantUser() {
		User adminUser = new AdministrativeAssistantUser();
		adminUser.setId(AA_ID);
		adminUser.setFirstName(AA_FIRST_NAME);
		adminUser.setLastName(AA_LAST_NAME);
		adminUser.setGender(AA_GENDER);
		adminUser.setDateOfBirth(AA_DATE);

		assertEquals(AA_ID, adminUser.getId());
		assertEquals(AA_FIRST_NAME, adminUser.getFirstName());
		assertEquals(AA_LAST_NAME, adminUser.getLastName());
		assertEquals(AA_GENDER, adminUser.getGender());
		assertEquals(AA_DATE, adminUser.getDateOfBirth());
		System.out.println(adminUser.toString());
	}

	private static final Long FACULTY_ID = 1L;
	private static final String FACULTY_FIRST_NAME = "Jane";
	private static final String FACULTY_LAST_NAME = "Doe";
	private static final Gender FACULTY_GENDER = Gender.FEMALE;
	private static final Date FACULTY_DATE = new GregorianCalendar(1977,
			Calendar.FEBRUARY, 11).getTime();

	private static final Long AA_ID = 1L;
	private static final String AA_FIRST_NAME = "Jane";
	private static final String AA_LAST_NAME = "Doe";
	private static final Gender AA_GENDER = Gender.FEMALE;
	private static final Date AA_DATE = new GregorianCalendar(1984,
			Calendar.JANUARY, 23).getTime();

}
