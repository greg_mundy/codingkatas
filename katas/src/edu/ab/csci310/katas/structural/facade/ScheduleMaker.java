package edu.ab.csci310.katas.structural.facade;

import java.util.HashMap;
import java.util.Map;

/**
 * The {@linkplain ScheduleMaker} class creates a schedule for a student.
 * 
 * @author gregorymundy
 *
 */
public class ScheduleMaker {
	private Map<Course, Room> courseRoom;
	private Course course;
	private Room room;

	public ScheduleMaker() {
		courseRoom = new HashMap<Course, Room>();
	}

	public HashMap<Course, Room> getSchedule() {
		return null;
	}
}
