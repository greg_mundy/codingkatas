package edu.ab.csci310.katas.structural.facade;

/**
 * The {@linkplain Room} class represents a classroom where courses are taught.
 * 
 * @author gregorymundy
 *
 */
public class Room {
	private Long number;
	private String building;

	/**
	 * 
	 * @return The room number;
	 */
	public Long getNumber() {
		return number;
	}

	/**
	 * 
	 * @param number
	 *            The room number;
	 */
	public void setNumber(Long number) {
		this.number = number;
	}

	/**
	 * 
	 * @return The name of the campus building.
	 */
	public String getBuilding() {
		return building;
	}

	/**
	 * 
	 * @param building
	 *            The name of the campus building;
	 */
	public void setBuilding(String building) {
		this.building = building;
	}
}