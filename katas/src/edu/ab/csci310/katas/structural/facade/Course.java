package edu.ab.csci310.katas.structural.facade;

/**
 * The {@linkplain Course} class represents an academic course at a higher
 * education institution.
 * 
 * @author gregorymundy
 *
 */
public class Course {
	private Long id;
	private String title;
	private String description;

	/**
	 * 
	 * @return The course's unique identifier.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The course's unique identifier.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The course title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param courseTitle
	 *            The course title.
	 */
	public void setTitle(String courseTitle) {
		this.title = courseTitle;
	}

	/**
	 * 
	 * @return The course description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 *            The course description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return id + " " + title + " " + description;
	}
}
