package edu.ab.csci310.katas.structural.adapter;

import java.util.Date;

/**
 * The {@linkplain User} abstract class describes a generic user that is managed
 * by the system.
 * 
 * @author gregorymundy
 *
 */
public abstract class User {
	private Long id;
	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private Gender gender;

	/**
	 * 
	 * @return The user's unique identifier.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The user's unique identifier.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The user's first name.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * @param firstName
	 *            The user's first name.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 
	 * @return The user's last name.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @param lastName
	 *            The user's last name.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 
	 * @return The user's date of birth.
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * 
	 * @param dateOfBirth
	 *            The user's date of birth.
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return (id + " "  + firstName + " "  + lastName + " "  + dateOfBirth);
	}

	/**
	 * 
	 * @return The user's position description.
	 */
	public abstract String getPositionDescription();

	/**
	 * 
	 * @return The user's gender.
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * 
	 * @param gender
	 *            The user's gender.
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}
}