package edu.ab.csci310.katas.structural.adapter;

/**
 * The {@linkplain FacultyUser} class extends the {@linkplain User} class and is
 * used for representing faculty members.
 * 
 * @author gregorymundy
 *
 */
public class FacultyUser extends User {

	@Override
	public String getPositionDescription() {
		return "Teach a variety of lower to upper level undergraduate courses "
				+ " in area(s) of expertise.";
	}
}
