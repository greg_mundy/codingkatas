package edu.ab.csci310.katas.structural.adapter;

/**
 * The {@linkplain AdministrativeAssistantUser} class represents an
 * administrative assistant user (formally referred to as a secretary). It
 * extends the {@linkplain User} class.
 * 
 * @author gregorymundy
 *
 */
public class AdministrativeAssistantUser {

}
