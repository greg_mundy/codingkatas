package edu.ab.csci310.katas.structural.adapter;

public enum Gender {
	MALE, FEMALE, UNDEFINED;
}
